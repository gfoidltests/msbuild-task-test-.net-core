﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace TestTask
{
    public class MyTask : Task
    {
        [Required]
        public string Name    { get; set; }

        public int Age        { get; set; }

        [Output]
        public string Message { get; private set; }
        //---------------------------------------------------------------------
        public override bool Execute()
        {
            this.Log.LogMessage(MessageImportance.High, $"----- Hello {this.Name} -----");
            this.Log.LogMessage($"Age: {this.Age.ToString()}");

            this.Message = $"Hello {this.Name}, aged {this.Age.ToString()}";

            return this.Name != "Adolf" && !this.Log.HasLoggedErrors;
        }
    }
}